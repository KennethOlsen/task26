﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PGM_online.Models;

namespace PGM_online.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {

            return View("MyFirstView");

        }

        public IActionResult SupervisorConfirmation()
        {
            
            return View("AddSupervisorConfirmation");

        }

        [HttpGet]
        public IActionResult AllSupervisors()
        {
            List<string> supervisorsInfo = new List<string>();

            foreach (Supervisor s in SupervisorGroup.CurrentSupervisors)
            {
                int? id = s?.Id;
                string name = s?.Name;
                string level = s?.Level;
                bool? available = s?.isAvailable;

                supervisorsInfo.Add($"ID: {id} Name: {name} Level: {level} Available: {available}");

            }

            return View("AllSupervisors", supervisorsInfo);
        }

        [HttpGet]
        public IActionResult SupervisorList()
        {
            List<Supervisor> supervisorsWithS = new List<Supervisor>();

            supervisorsWithS = SupervisorGroup.ShortSupervisorList().FindAll(s => s.Name.StartsWith("S"));
        
            return View("SupervisorList", supervisorsWithS);
        }



        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {

                SupervisorGroup.CurrentSupervisors.Add(supervisor);

                return View("AddSupervisorConfirmation", supervisor);
            }

            else
            {
                return View("MyFirstView");
            }

            
        }


    }

}
