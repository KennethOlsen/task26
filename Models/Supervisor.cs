﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PGM_online.Models
{
    public class Supervisor
    {
        [Required(ErrorMessage = "Please enter a number")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please enter a valid whole number")]
        public int? Id { get; set; } = 55555;

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "Nameless";

        [Required(ErrorMessage = "Please indicate availability")]
        public bool? isAvailable { get; set; } = false;

        [Required(ErrorMessage = "Please enter a competence level")]
        public string Level { get; set; } = "none";

    }
}
