﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGM_online.Models
{
    public static class SupervisorGroup
    {
        public static List<Supervisor> CurrentSupervisors = new List<Supervisor>()
            {
             new Supervisor { Id = 1, isAvailable = true, Level = "Super Pro", Name = "Albert Einstein" },
             new Supervisor { Id = 2, isAvailable = false, Level = "OK", Name = "Nikola Tesla" },
             new Supervisor { Id = 3, isAvailable = true, Level = "Elite", Name = "Marie Curie" },
             new Supervisor { Id = 4, isAvailable = true, Level = "Master", Name = "Stephen Hawking" }
            };

        public static List<Supervisor> Supervisors
        {
            get { return CurrentSupervisors; }
        }


        public static List<Supervisor> GetSampleSupervisorData()
                            
        {

            Supervisor supervisor1 = new Supervisor { Id = 1, isAvailable = true, Level = "Pro", Name = "Pedro Degro" };
            Supervisor supervisor2 = new Supervisor { Id = 2, isAvailable = false, Level = "OK", Name = "Sandy Pandy" };
            Supervisor supervisor3 = new Supervisor { Id = 3, isAvailable = true, Level = "Average", Name = "Lou Blou" };
            Supervisor supervisor4 = new Supervisor { Id = 4, isAvailable = true, Level = "New", Name = "Jack White" };
            Supervisor supervisor5 = new Supervisor { Id = 5, isAvailable = false, Level = "Top", Name = "Jack Black" };
            Supervisor supervisor6 = new Supervisor { Id = 6, isAvailable = false, Level = "Average", Name = "Jack Grey" };
            Supervisor supervisor7 = new Supervisor { Id = 7, isAvailable = true, Level = "Super", Name = "Superwoman" };
            Supervisor supervisor8 = new Supervisor();

            List<Supervisor> supervisors = new List<Supervisor>();

            supervisors.Add(supervisor1);
            supervisors.Add(supervisor2);
            supervisors.Add(supervisor3);
            supervisors.Add(supervisor4);
            supervisors.Add(supervisor5);
            supervisors.Add(supervisor6);
            supervisors.Add(supervisor7);
            supervisors.Add(supervisor8);
            supervisors.Add(null);

            return supervisors;
        }

        public static List<Supervisor> ShortSupervisorList()
        {
            Supervisor supervisor11 = new Supervisor { Id = 1, isAvailable = true, Level = "Super Pro", Name = "Albert Einstein" };
            Supervisor supervisor12 = new Supervisor { Id = 2, isAvailable = false, Level = "OK", Name = "Nikola Tesla" };
            Supervisor supervisor13 = new Supervisor { Id = 3, isAvailable = true, Level = "Elite", Name = "Marie Curie" };
            Supervisor supervisor14 = new Supervisor { Id = 4, isAvailable = true, Level = "Master", Name = "Stephen Hawking" };

            List<Supervisor> shortList = new List<Supervisor>();

            shortList.Add(supervisor11);
            shortList.Add(supervisor12);
            shortList.Add(supervisor13);
            shortList.Add(supervisor14);

            return shortList;
        }

    }
}
